var Imap = require('imap'),
    inspect = require('util').inspect;
var fs      = require('fs');
var base64  = require('base64-stream');
var Base64Decode = base64.Base64Decode;
var config = JSON.parse(fs.readFileSync('./config.json').toString());
var imap = new Imap({
  user: config.user,
  password: config.password,
  host: 'outlook.office365.com',
  port: 993,
  tls: true
});

function openInbox(cb) {
  imap.openBox('INBOX', true, cb);
}

function toUpper(thing) { return thing && thing.toUpperCase ? thing.toUpperCase() : thing;}

function findAttachmentParts(struct, attachments) {
  attachments = attachments ||  [];
  for (var i = 0, len = struct.length, r; i < len; ++i) {
    if (Array.isArray(struct[i])) {
      findAttachmentParts(struct[i], attachments);
    } else {
      if (struct[i].disposition && ['INLINE', 'ATTACHMENT'].indexOf(toUpper(struct[i].disposition.type)) > -1) {
        attachments.push(struct[i]);
      }
    }
  }
  return attachments;
}

function buildAttMessageFunction(attachment, header) {
  console.log(attachment)
  var filename = attachment.params.name + '_' + header.date;
  var type = attachment.type + '/' + attachment.subtype;
  var encoding = attachment.encoding;

  var path = './download/' + filename;
  var date = '';

  // if (!filename.endsWith(".xlsx")) {
  //   return function () {
  //     console.log('Not a xlsx file');
  //   };
  // }

  return function (msg, seqno) {
    var prefix = '(#' + seqno + ') ';
    msg.on('body', function(stream, info) {
      //Create a write stream so that we can stream the attachment to file;
      console.log(prefix + 'Streaming this attachment to file', filename, info);

      try {
        fs.accessSync(path, fs.F_OK);
        console.log ('file found, deleting');
        fs.unlink(path, function (err, result) {
          write(stream);
        });
      }
      catch (e) {
        console.log ('new file');
        write(stream);
      }
    });
    function write(stream) {
      var writeStream = fs.createWriteStream(path);
      writeStream.on('finish', function() {
        console.log(prefix + 'Done writing to file %s', filename);
        console.log(prefix, date, path);
      });

      //stream.pipe(writeStream); this would write base64 data to the file.
      //so we decode during streaming using
      if (toUpper(encoding) === 'BASE64') {
        //the stream is base64 encoded, so here the stream is decode on the fly and piped to the write stream (file)
        stream.pipe(new Base64Decode({prefix:"data:" + type + ";base64,"})).pipe(writeStream);
      } else  {
        //here we have none or some other decoding streamed directly to the file which renders it useless probably
        stream.pipe(writeStream);
      }
    }

  };
}

exports = module.exports = {
  run: function (cb) {
    imap.once('ready', function() {
      imap.openBox('INBOX', false, function(err, box) {
        // if (err) throw err;
        console.log('inbox')
        // imap.seq.search( [['FROM', 'ddanphung@gmail.com'], '!SEEN'], function (err, results) {
        imap.seq.search( [['FROM', 'fptplay@fpt.com.vn']], function (err, results) {
          console.log (results.length);
          if (err || !results || results.length == 0){
            return imap.end();
          }
          var f = imap.seq.fetch(results, {
            bodies: 'HEADER.FIELDS (FROM TO SUBJECT DATE)',
            markSeen: true,
            struct: true
          });
          f.on('message', function (msg, seqno) {
            console.log('Message #%d', seqno);
            var prefix = '(#' + seqno + ') ';
            var header;
            msg.on('body', function(stream, info) {
              var buffer = '';
              stream.on('data', function(chunk) {
                buffer += chunk.toString('utf8');
              });
              stream.once('end', function() {
                console.log(prefix + 'Parsed header: %s', JSON.stringify(Imap.parseHeader(buffer)));
                header = Imap.parseHeader(buffer);
              });
            });
            msg.once('attributes', function(attrs) {
              var attachments = findAttachmentParts(attrs.struct);
              console.log(prefix + 'Has attachments: %d', attachments.length);
              for (var i = 0, len=attachments.length ; i < len; ++i) {
                var attachment = attachments[i];
                console.log(prefix + 'Fetching attachment %s', attachment.params.name);
                var f = imap.fetch(attrs.uid , {
                  bodies: [attachment.partID],
                  struct: true
                });
                //build function to process attachment message
                if (attachments.length > 0) {
                  f.on('message', buildAttMessageFunction(attachment, header));
                }
              }
            });
            msg.once('end', function() {
              console.log(prefix + 'Finished email');
            });
          });
          f.once('error', function(err) {
            console.log('Fetch error: ' + err);
          });
          f.once('end', function() {
            console.log('Done fetching all messages!');
            imap.end();
          });
        })
      });
    });

    imap.once('error', function(err) {
      console.log(err);
    });

    imap.once('end', function() {
      console.log('Connection ended');
      cb();
    });

    console.log(1)
    imap.connect();
  }
}

exports.run(function() {
  console.log('Done fetch email');
})
